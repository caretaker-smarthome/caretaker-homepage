<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);


// GET SELECTED PAGE
if (isset($_GET["p"])) {
    $selectedPage = $_GET["p"];
} else {
    $selectedPage = "home";
}
// END GET SELECTED PAGE


function getPathToSpecificStyles(): string
{
    global $selectedPage;

    switch ($selectedPage) {
        default:
            return "./pages/home/styles.css";

        case "home":
            return "./pages/home/styles.css";

        case "doc":
            return "./pages/documentation/styles.css";
    }
}

function bodyRender()
{
    global $selectedPage;

    switch ($selectedPage) {
        default:
            require_once __DIR__ . '/pages/home/index.php';
            break;

        case "home":
            require_once __DIR__ . '/pages/home/index.php';
            break;

        case "doc":
            require_once __DIR__ . '/pages/documentation/index.php';
            break;
    }
}

?>

        <!DOCTYPE html>
        <html lang="en">

        <head>

            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="">
            <meta name="author" content="">

            <title>Caretaker - Smarthome</title>

            <link rel="shortcut icon" type="image/png" href="./img/simpleIcon.png" />

            <!-- Bootstrap core CSS -->
            <link rel="stylesheet" type="text/css" href='./vendor/twbs/bootstrap/dist/css/bootstrap.min.css'>
            <link rel="stylesheet" href="./vendor/fortawesome/font-awesome/css/all.min.css" rel="stylesheet">

            <!-- Custom fonts for this template -->
            <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

            <!-- Plugin CSS -->
            <link rel="stylesheet" href="device-mockups/device-mockups.min.css">

            <!-- Custom styles for this template -->
            <link href="css/new-age.css" rel="stylesheet">
            <link href="css/styles.css" rel="stylesheet">

            <!-- Specific styles for selected page -->
            <link href="<?php echo getPathToSpecificStyles() ?>" rel="stylesheet">


        </head>

        <body id="page-top">

            <?php require_once __DIR__ . '/components/navbar.php'; ?>
            <?php require_once __DIR__ . '/components/workInProgressModal.php'; ?>

            <?php bodyRender(); ?>

            <?php require_once __DIR__ . '/components/footer.php'; ?>

            <!-- Bootstrap core JavaScript -->
            <script type="text/javascript" src="./vendor/components/jquery/jquery.min.js"></script>
            <script type="text/javascript" src="./vendor/alexandermatveev/popper-bundle/AlexanderMatveev/PopperBundle/Resources/public/popper.min.js"></script>
            <script type="text/javascript" src="./vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>

            <!-- Custom scripts for this template -->
            <script type="text/javascript" src="./js/new-age.js"></script>
            <script type="text/javascript" src="./js/script.js"></script>

        </body>

        </html>