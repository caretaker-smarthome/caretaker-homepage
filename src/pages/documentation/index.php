<?php

namespace page\documentation;

// GET SELECTED DOCUMENTATION
if (isset($_GET["d"])) {
    $selectedDoc = $_GET["d"];
} else {
    $selectedDoc = "home";
}
// END GET SELECTED DOCUMENTATION


function getPathToSpecificStyles(): string
{
    global $selectedDoc;

    switch ($selectedDoc) {
        default:
            return __DIR__ . "/pages/home/styles.css";

        case "home":
            return __DIR__ . "/pages/caretaker_home/styles.css";
    }
}

function bodyRender()
{
    global $selectedDoc;

    switch ($selectedDoc) {
        default:
            require_once __DIR__ . '/pages/caretaker_home/index.php';
            break;

        case "home":
            require_once __DIR__ . '/pages/caretaker_home/index.php';
            break;
    }
}

?>


        <?php bodyRender(); ?>
