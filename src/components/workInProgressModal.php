<div class="modal fade" id="workingInProgressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="alert alert-warning" role="alert">
            <p>
                <i class="fas fa-exclamation-triangle"></i>
                <span>
                    This website is under construction. It is therefore normal to sometimes come across empty sections. We're doing our best to finalize it as soon as possible
                </span>
            </p>
        </div>
    </div>
</div>